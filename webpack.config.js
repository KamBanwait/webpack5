const path = require('path')

// env is passed in via webpack and we set if we're running prod/dev within package.json
module.exports = (env) => {
  const mode = env.production === true ? 'production' : 'development'

  return {
    mode: mode,
    entry: './src/index.js',
    output: {
      filename: 'bundle.js',
      path: path.resolve(__dirname, 'public')
    },

    module: {
      rules: [
        {
          // get all files ending in js
          test: /\.js$/,
          // we don't want to include other peoples modules in our transpiling
          exclude: /node_modules/,
          use: {
            // without additional settings, this will reference .bablerc
            loader: 'babel-loader'
          }
        }
      ]
    },

    devtool: 'source-map', // needs to be set via mode

    devServer: {
      // where to serve the content from (looks for index.html from our output)
      contentBase: './public'
    }
  }
}
